import { Component, Input } from '@angular/core';

import { Nav } from '../../services/nav.interface';

@Component({
  selector: 'app-nav-item',
  templateUrl: './nav-item.component.html',
  styleUrls: ['./nav-item.component.scss']
})
export class NavItemComponent {
  @Input()
  item: Nav;
  status = 'off';
  constructor(
  ) { }

  toggleNav(n) {
    if (!n.children.length) { return; }
    this.status = (this.status === 'on') ? 'off' : 'on';
  }
}
