import {Component, OnInit } from '@angular/core';

import { Nav } from '../../services/nav.interface';

import { NavService } from '../../services/nav.service';
import { UtilityService } from '../../util/utility';

@Component({
  selector: 'app-nav-list',
  templateUrl: './nav-list.component.html',
  styleUrls: ['./nav-list.component.scss']
})
export class NavListComponent implements OnInit {
 nodes: Nav[];
  constructor(
    private navService: NavService,
    private util: UtilityService
  ) {}

  async ngOnInit() {
    const data = await this.navService.getNavData();
    this.nodes = this.util.transformData(data);
  }
}
