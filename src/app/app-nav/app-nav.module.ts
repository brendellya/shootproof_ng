import { NgModule} from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';

import { NavService } from '../services/nav.service';

import { NavListComponent } from './nav-list/nav-list.component';
import { NavItemComponent } from './nav-item/nav-item.component';

@NgModule({
    declarations: [
        NavListComponent,
        NavItemComponent
    ],
    imports: [
        BrowserModule,
        CommonModule
    ],
    providers: [
        NavService
    ],
    exports: [
        NavListComponent
    ]
})
export class AppNavModule {}
