import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';

import { AppComponent } from './app.component';

import { AppNavModule } from './app-nav/api';


@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    CommonModule,
    /*
      Custom
     */
    AppNavModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
