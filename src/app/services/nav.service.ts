import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class NavService {

  constructor(
    private http: HttpClient
  ) { }

  getNavData(): Promise<Object> {
    return this.http.get('../../assets/testdata.json').toPromise();
  }
}
