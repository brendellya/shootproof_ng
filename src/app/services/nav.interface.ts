export interface Nav {
    id: number;
    name: string;
    thumbnail: any;
    parent: string;
    children?: any;
}
