import { Injectable } from '@angular/core';
import { Nav } from '../services/nav.interface';

@Injectable({
  providedIn: 'root'
})
export class UtilityService {
    transformData(data): Nav[] {
        const sortNodes = (a, b) => {
            const aFirstSort = a['parent'];
            const bFirstSort = b['parent'];

            if (aFirstSort < bFirstSort) {
                return -1;
            } else if (bFirstSort === null) {
                return 1;
            } else if (aFirstSort === null) {
                return -1;
            }
        };
        const findSubs = (parents, index) => {
            return parents.find((p) => p.id === index);
        };
        const findParentNodes = (nodes) => nodes.reduce((memo, node) => {
            if (node.parent === null) {
                memo.push(Object.assign({
                    children: []
                }, node));
            }
            return memo;
        }, []);
        const findChildNodes = (nodes, parents) => nodes.reduce((memo, node) => {
            memo = [...parents];
            if (node.parent !== null) {
                const parentIndex = node.parent;
                const parent = findSubs(memo, parentIndex);
                node.children = [];

                //
                if (parent) {
                    parent.children.push(node);
                } else {
                    parents.forEach((p) => {
                        if (p.children.length) {
                            const subParent = findSubs(p.children, parentIndex);
                            subParent.children.push(node);
                        }
                    });
                }
            }
            return memo;
        }, []);

        const sortedResult = data.sort(sortNodes);
        return findChildNodes(sortedResult, findParentNodes(sortedResult));
    }
}
